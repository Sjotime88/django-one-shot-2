from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from todos.models import TodoList, TodoItem
from django.views.generic.edit import CreateView

class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"
    context_object_name = "todo_list" # control what db data is called in template

    # def get_context_data(self, *args, **kwargs):
    #     context = super().get_context_data(*args, **kwargs)
    #     print(context)
    #     return context

    def get_queryset(self):
        return TodoList.objects.all()


class TodoDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"
    context_object_name = "todo_item"

    def get_queryset(self):
        return TodoList.objects.all()


class TodoCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])

    # If the to-do list is successfully created, it should redirect to the detail page for that to-do list
